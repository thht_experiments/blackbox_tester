# Blackbox Testing Script

This is the source code for the paradigm used to test the timing accuracy of
the [o_ptb](https://gitlab.com/thht/o_ptb).

## What is included?

This repository includes the source code to run the paradigm. It needs a
current version of Matlab, the [o_ptb](https://gitlab.com/thht/o_ptb) and the
[Psychtoolbox](psychtoolbox.org/) to run it.

The experiment consists of a white circle, a white noise and a trigger, all sent
at the same time. Accuracy can then be evaluated by recording the onset time
of these events with an oscilloscope of specialized equipment such as the
[Blackbox Toolkit](https://www.blackboxtoolkit.com/)

The `data` folder also includes the hardware configuration of the two systems
this script was run on as reported in the preprint covering the o_ptb: [https://psyarxiv.com/g4nbx/]

The respective data and analysis code can be found [here](https://gitlab.com/thht/blackbox_tester_usbstim/-/tree/article)
