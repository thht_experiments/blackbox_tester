function block()
%% init
cfg = exp.init.prepare_cfg;
ptb_config = exp.init.config_ptb();

ptb = o_ptb.PTB.get_instance(ptb_config);

ptb.setup_screen();
ptb.setup_audio();
ptb.setup_trigger();

%% prepare stims...
snd = o_ptb.stimuli.auditory.WhiteNoise(0.1);
trigger = 255;
visual = o_ptb.stimuli.visual.FilledCircle(100, o_ptb.constants.PTB_Colors.white);

%% be prepared...
ptb.draw(o_ptb.stimuli.visual.Text('Press key to start'));
ptb.flip();
KbWait();
ptb.flip();

%% stim loop
tstamp = GetSecs();

for idx_block = 1:cfg.n_trials
  ptb.draw(visual);
  ptb.prepare_audio(snd);
  ptb.prepare_trigger(trigger);
  
  ptb.schedule_audio();
  ptb.schedule_trigger();
  ptb.play_on_flip();
  
  tstamp = ptb.flip(tstamp + cfg.isi + RandLim(1, -cfg.jitter, cfg.jitter));
  ptb.flip(tstamp + cfg.visual_duration);
end %for

sca
end

