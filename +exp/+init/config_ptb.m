function ptb_config = config_ptb()
%CONFIG_PTB Summary of this function goes here
%   Detailed explanation goes here
ptb_config = o_ptb.PTB_Config();
ptb_config.window_scale = 0.2;
ptb_config.flip_horizontal = false;
ptb_config.fullscreen = true;
ptb_config.skip_sync_test = false;
ptb_config.force_datapixx = false;
ptb_config.background_color = o_ptb.constants.PTB_Colors.black;
ptb_config.labjacktrigger_config.channel_group = labjack.Labjack.ChannelGroup.FIO;

ptb_config.real_experiment_sbg_cdk(false);

commandwindow
end

